//
//  RefuelViewController.swift
//  Fuel
//
//  Created by Svetlana Shapovalenko on 19.10.2020.
//

import UIKit
import RealmSwift

class RefuelViewController: UIViewController {

    @IBOutlet var AddFuelView: UIView!
    
    @IBOutlet weak var mileage: UITextField!
    @IBOutlet weak var volume: UITextField!
    @IBOutlet weak var price: UITextField!
    @IBOutlet weak var total: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func addFuel(_ sender: Any) {
        let newFuel = Refuel()
        newFuel.mileage = Int(mileage.text ?? "") ?? 0
        newFuel.volume = Double(volume.text ?? "") ?? 0
        newFuel.price = Double(price.text ?? "") ?? 0
        newFuel.total = newFuel.volume * newFuel.price
        
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(newFuel)
            }
            
        } catch {
            print("Error instaling new Realm: \(error)")
        }
        
        AddFuelView.endEditing(true)
        AddFuelView.isHidden = true

    }
    
}
