//
//  Refuel.swift
//  Fuel
//
//  Created by Svetlana Shapovalenko on 19.10.2020.
//

import Foundation
import RealmSwift

class Refuel: Object {
    @objc dynamic var mileage: Int = 0
    @objc dynamic var volume: Double = 0.0
    @objc dynamic var price: Double = 0.0
    @objc dynamic var total: Double = 0.0
    //@objc dynamic var comment: String = ""
    
    override init() {
        super.init()
    }
    
    init(mileage: Int, volume: Double, price: Double, total: Double) {
        self.mileage = mileage
        self.volume = volume
        self.price = price
        self.total = total
        
    }
}
